mkdir build
docker run \
-v ${PWD}/build:/docs/build \
-v ${PWD}/source:/docs/source \
-v ${PWD}/Makefile:/docs/Makefile \
-t jupyter:latest \
make html
# Add -W option to treat warnings as errors