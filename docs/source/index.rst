.. notes documentation master file, created by
   sphinx-quickstart on Fri Jun  4 23:26:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to notes's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   mechanical/formula.rst
   notes/notes.rst
   matplotlib/index.rst


How to build the docs
=====================

.. code-block:: bash

   make html




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
   
.. toctree::
   auto_examples/index.rst


.. toctree::
   :glob:
   :maxdepth: 5

   nb_examples/index.rst